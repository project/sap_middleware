<?php

namespace Drupal\sap_middleware;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface;
use Drupal\sap_middleware\Event\OrderPayloadEvent;
use Drupal\sap_middleware\Event\OrderSyncedEvent;
use Drupal\sap_middleware\Event\OrderSyncFailedEvent;
use Drupal\sap_middleware\Event\OrderSyncStatesEvent;
use Elastica\Exception\Connection\GuzzleException;
use GuzzleHttp\ClientInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Service class that contains helper methods for the SAP integration.
 */
class SapMiddlewareService {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP client to post to sap cpi.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructor for SapMiddlewareService.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, ClientInterface $http_client, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, QueueFactory $queue_factory) {
    $this->logger = $logger_factory->get('sap_middleware');
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $this->queue = $queue_factory->get('sap_sync_orders');
  }

  /**
   * Queue orders for syncing to SAP.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface|null $client
   *   The client entity to use to sync the orders.
   */
  public function syncOrders(SapMiddlewareClientInterface $client = NULL) {
    if (!$client) {
      $client = $this->getClient();
    }
    // If a client could not be fetched, do nothing.
    if (!$client) {
      return;
    }

    $plugin = $client->getPlugin();
    // Add filters and fetch orders that haven't been synced to SAP.
    $order_ids = $this->getOrdersToSync([
      'state' => [
        'value' => $this->getOrderStatesToSync($client),
        'operator' => 'IN',
      ],
      'store_id' => [
        'value' => $plugin->getSupportedStores(),
        'operator' => 'IN',
      ],
      'type' => [
        'value' => $plugin->getSupportedOrderTypes(),
        'operator' => 'IN',
      ],
      $plugin->getSapOrderIdField() => [
        'value' => NULL,
        'operator' => 'IS NULL',
      ],
    ]);

    foreach ($order_ids as $order_id) {
      $this->queue->createItem(['order_id' => $order_id]);
    }
  }

  /**
   * Get all completed orders which haven't been sent to SAP yet.
   *
   * @param array $args
   *   An array of arguments to pass to the entity query.
   *   ie. [
   *     'field_some_order_field' => ['value' => 5, 'operator' => 'IN'],
   *     'field_another_order_field' => ['value' => 'test', 'operator' => 'IN'],
   *   ].
   * @param int $limit
   *   The number of records to fetch.
   *
   * @return array
   *   An array of order IDs.
   */
  public function getOrdersToSync(array $args, int $limit = 50): array {
    $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
    if ($args) {
      foreach ($args as $field_name => $value) {
        $query->condition($field_name, $value['value'], $value['operator']);
      }
    }
    $query->accessCheck(FALSE);
    $query->range(0, $limit);

    return $query->execute();
  }

  /**
   * Authenticate and return an access token for a specific client middleware.
   *
   * @param string|null $client_id
   *   The ID of the client to authenticate with.
   *
   * @return string
   *   The access token if the Oauth was successful.
   */
  public function getAccessToken(string $client_id = NULL) {
    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client */
    $client = $this->getClient($client_id);
    if ($client) {
      try {
        $client_plugin = $client->getPlugin();
        return $client_plugin->getAccessToken();
      }
      catch (\Exception $e) {
        throw new \RuntimeException(sprintf('An error was encountered while fetching an access token from the SAP Middleware Client. The error code was: %s and the error message was: %s.', $e->getCode(), $e->getMessage()));
      }
    }

    return FALSE;
  }

  /**
   * Send order to SAP.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param string|null $client_id
   *   The ID of the client used to send the order to SAP.
   * @param string|null $access_token
   *   The access token received after authentication.
   *
   * @return string|false
   *   The SAP order ID if order was synced successfully.
   */
  public function sendOrder(OrderInterface $order, string $client_id = NULL, string $access_token = NULL) {
    // Get the client plugin first.
    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client */
    $client = $this->getClient($client_id);
    if (!$client) {
      throw new \InvalidArgumentException(sprintf('The client ID "%s" is invalid.', $client_id));
    }

    // Get the order payload.
    $client_plugin = $client->getPlugin();
    $payload = $this->getOrderPayload($client, $order);

    try {
      // If no access token was passed, create one via the client.
      if (!$access_token) {
        $access_token = $client_plugin->getAccessToken();
      }

      // Now, try sending the order to SAP.
      $result = $client_plugin->sendOrder($payload, $access_token);
      $sap_order_id = $result['sap_order_id'];

      // Log the successful sync details to the order.
      $this->logStorage->generate($order, 'sap_middleware_order_sync_successful', [
        'order_id' => $order->id(),
        'sap_order_id' => $sap_order_id,
      ])->save();

      // Dispatch the order synced event.
      $event = new OrderSyncedEvent($client, $order, $payload, $result);
      $this->eventDispatcher->dispatch($event, OrderSyncedEvent::EVENT_NAME);
    }
    catch (\Exception $e) {
      // Log the sync exception details to the order.
      $this->logStorage->generate($order, 'sap_middleware_order_sync_failed', [
        'order_id' => $order->id(),
        'exception_message' => $e->getMessage(),
      ])->save();

      // Dispatch the order sync failed event.
      $event = new OrderSyncFailedEvent($client, $order, $payload, $e);
      $this->eventDispatcher->dispatch($event, OrderSyncFailedEvent::EVENT_NAME);

      throw new \RuntimeException(sprintf('There was an error while syncing the order to SAP. The error code was: %s and the error message was: %s.', $e->getCode(), $e->getMessage()));
    }

    return $sap_order_id;
  }

  /**
   * Fetch the order payload given a client and order.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   The SAP Middleware client plugin.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return array
   *   The order payload.
   */
  public function getOrderPayload(SapMiddlewareClientInterface $client, OrderInterface $order) {
    $client_plugin = $client->getPlugin();
    $payload = $client_plugin->getOrderPayload($order);
    // Dispatch the order payload event to allow modules to alter it.
    $event = new OrderPayloadEvent($client, $order, $payload);
    $this->eventDispatcher->dispatch($event, OrderPayloadEvent::EVENT_NAME);

    return $event->getPayload();
  }

  /**
   * Fetch and return an SAP Middleware Client if an ID is passed.
   *
   * If no ID is passed, it will return the most recent enabled client.
   *
   * @param string|null $client_id
   *   The ID of the client to load.
   *
   * @return \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface|null
   *   The loaded SAP Middleware Client entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClient(string $client_id = NULL) {
    if ($client_id) {
      return $this->entityTypeManager->getStorage('sap_middleware_client')->load($client_id);
    }

    // There should only be one client enabled at a time.
    $clients = $this->entityTypeManager->getStorage('sap_middleware_client')->loadByProperties([
      'status' => TRUE,
    ]);
    // In the case that multiple clients are fetched, return the last one.
    if ($clients) {
      return end($clients);
    }

    return NULL;
  }

  /**
   * Return the order states that should be synced.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   An SAP Middleware Client entity.
   *
   * @return array
   *   An array of order states.
   */
  public function getOrderStatesToSync(SapMiddlewareClientInterface $client) {
    $states = [
      'completed',
    ];

    // Allow other modules to modify this.
    $event = new OrderSyncStatesEvent($client, $states);
    $this->eventDispatcher->dispatch($event, OrderSyncStatesEvent::EVENT_NAME);

    return $event->getStates();
  }

}
