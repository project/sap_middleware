<?php

namespace Drupal\sap_middleware\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the SAP Middleware Client plugin annotation object.
 *
 * Plugin namespace: Plugin\SapMiddleware\Client.
 *
 * @Annotation
 */
class SapMiddlewareClient extends Plugin {

  use StringTranslationTrait;

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The client label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The supported modes.
   *
   * An array of mode labels keyed by machine name.
   *
   * @var string[]
   */
  public $modes;

  /**
   * Constructs a new SapMiddlewareClient object.
   *
   * @param array $values
   *   The annotation values.
   */
  public function __construct(array $values) {
    if (empty($values['modes'])) {
      $values['modes'] = [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ];
    }

    parent::__construct($values);
  }

}
