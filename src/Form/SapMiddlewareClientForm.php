<?php

namespace Drupal\sap_middleware\Form;

use Drupal\commerce\InlineFormManager;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sap_middleware\SapMiddlewareClientManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The SAP Middleware settings form.
 */
class SapMiddlewareClientForm extends EntityForm {

  /**
   * The SAP Middleware Client plugin manager.
   *
   * @var \Drupal\sap_middleware\SapMiddlewareClientManager
   */
  protected $pluginManager;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new SapMiddlewareClientForm object.
   *
   * @param \Drupal\sap_middleware\SapMiddlewareClientManager $plugin_manager
   *   The SAP Middleware Client plugin manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(SapMiddlewareClientManager $plugin_manager, InlineFormManager $inline_form_manager, EntityFieldManagerInterface $entity_field_manager = NULL) {
    $this->pluginManager = $plugin_manager;
    $this->inlineFormManager = $inline_form_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.sap_middleware_client'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $type */
    $type = $this->entity;
    $plugins = array_column($this->pluginManager->getDefinitions(), 'label', 'id');

    // Use the first available plugin as the default value.
    if (!$type->getPluginId()) {
      $plugin_ids = array_keys($plugins);
      $plugin = reset($plugin_ids);
      $type->setPluginId($plugin);
    }
    // The form state will have a plugin value if #ajax was used.
    $plugin = $form_state->getValue('plugin', $type->getPluginId());
    // Pass the plugin configuration only if the plugin hasn't been changed via
    // #ajax.
    $plugin_configuration = $type->getPluginId() == $plugin ? $type->getPluginConfiguration() : [];

    $wrapper_id = Html::getUniqueId('sap-middleware-client-form');
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';

    $form['#tree'] = TRUE;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\sap_middleware\Entity\SapMiddlewareClient::load',
      ],
      '#disabled' => !$type->isNew(),
    ];
    $form['plugin'] = [
      '#type' => 'radios',
      '#title' => $this->t('Plugin'),
      '#options' => $plugins,
      '#default_value' => $plugin,
      '#required' => TRUE,
      '#disabled' => !$type->isNew(),
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];
    $inline_form = $this->inlineFormManager->createInstance('plugin_configuration', [
      'plugin_type' => 'sap_middleware_client',
      'plugin_id' => $plugin,
      'plugin_configuration' => $plugin_configuration,
    ]);
    $form['configuration']['#inline_form'] = $inline_form;
    $form['configuration']['#parents'] = ['configuration'];
    $form['configuration'] = $inline_form->buildInlineForm($form['configuration'], $form_state);
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $type->status(),
    ];

    return $form;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Only allow a single client to be enabled at one time.
    $values = $form_state->getValues();
    if (!$values['status']) {
      return;
    }

    $clients = $this->entityTypeManager->getStorage('sap_middleware_client')->loadMultiple();
    foreach ($clients as $client) {
      /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClient $client */
      // Skip if the client is the same as the current entity.
      if ($this->entity->id() === $client->id()) {
        continue;
      }

      if ($client->status()) {
        $form_state->setError($form, $this->t('Another SAP Middleware Client ("@client_id") is already enabled. You can only have 1 client enabled at a time. Please disable the client and try again.', [
          '@client_id' => $client->id(),
        ]));
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $type */
    $type = $this->entity;
    $type->setPluginConfiguration($form_state->getValue(['configuration']));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $this->messenger()->addMessage($this->t('Saved the %label SAP Middleware Client.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.sap_middleware_client.collection');
  }

}
