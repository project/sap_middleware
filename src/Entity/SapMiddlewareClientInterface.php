<?php

namespace Drupal\sap_middleware\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the interface for SAP Middleware Clients.
 *
 * This configuration entity stores configuration for SAP Middleware Client
 * plugins.
 */
interface SapMiddlewareClientInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Gets the SAP Middleware Client plugin.
   *
   * @return \Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientInterface
   *   The SAP Middleware Client plugin.
   */
  public function getPlugin();

  /**
   * Gets the SAP Middleware Client plugin ID.
   *
   * @return string
   *   The SAP Middleware Client plugin ID.
   */
  public function getPluginId();

  /**
   * Sets the SAP Middleware Client plugin ID.
   *
   * @param string $plugin_id
   *   The SAP Middleware Client plugin ID.
   *
   * @return $this
   */
  public function setPluginId($plugin_id);

  /**
   * Gets the SAP Middleware Client plugin configuration.
   *
   * @return array
   *   The SAP Middleware Client plugin configuration.
   */
  public function getPluginConfiguration();

  /**
   * Sets the SAP Middleware Client plugin configuration.
   *
   * @param array $configuration
   *   The SAP Middleware Client plugin configuration.
   *
   * @return $this
   */
  public function setPluginConfiguration(array $configuration);

}
