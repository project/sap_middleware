<?php

namespace Drupal\sap_middleware\Entity;

use Drupal\commerce\CommerceSinglePluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the SAP Middleware Client entity class.
 *
 * @ConfigEntityType(
 *   id = "sap_middleware_client",
 *   label = @Translation("SAP Middleware Client"),
 *   label_collection = @Translation("SAP Middleware Clients"),
 *   label_singular = @Translation("SAP Middleware Client"),
 *   label_plural = @Translation("SAP Middleware Clients"),
 *   label_count = @PluralTranslation(
 *     singular = "@count SAP Middleware Client",
 *     plural = "@count SAP Middleware Clients",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\sap_middleware\SapMiddlewareClientListBuilder",
 *     "form" = {
 *       "add" = "Drupal\sap_middleware\Form\SapMiddlewareClientForm",
 *       "edit" = "Drupal\sap_middleware\Form\SapMiddlewareClientForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "sap_middleware_client",
 *   admin_permission = "administer sap_middleware_client",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "plugin",
 *     "configuration",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/sap-middleware-client/add",
 *     "edit-form" = "/admin/config/services/sap-middleware-client/manage/{sap_middleware_client}",
 *     "delete-form" = "/admin/config/services/sap-middleware-client/manage/{sap_middleware_client}/delete",
 *     "collection" =  "/admin/config/services/sap-middleware-client"
 *   }
 * )
 */
class SapMiddlewareClient extends ConfigEntityBase implements SapMiddlewareClientInterface {

  /**
   * The SAP Middleware Client ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The SAP Middleware Client label.
   *
   * @var string
   */
  protected $label;

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The plugin collection that holds the SAP Middleware Client plugin.
   *
   * @var \Drupal\commerce\CommerceSinglePluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId($plugin_id) {
    $this->plugin = $plugin_id;
    $this->configuration = [];
    $this->pluginCollection = NULL;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfiguration(array $configuration) {
    $this->configuration = $configuration;
    $this->pluginCollection = NULL;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'configuration' => $this->getPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    // Invoke the setters to clear related properties.
    if ($property_name == 'plugin') {
      $this->setPluginId($value);
    }
    elseif ($property_name == 'configuration') {
      $this->setPluginConfiguration($value);
    }
    else {
      return parent::set($property_name, $value);
    }
  }

  /**
   * Gets the plugin collection that holds the SAP Middleware Client plugin.
   *
   * Ensures the plugin collection is initialized before returning it.
   *
   * @return \Drupal\commerce\CommerceSinglePluginCollection
   *   The plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $plugin_manager = \Drupal::service('plugin.manager.sap_middleware_client');
      $this->pluginCollection = new CommerceSinglePluginCollection($plugin_manager, $this->plugin, $this->configuration, $this);
    }
    return $this->pluginCollection;
  }

}
