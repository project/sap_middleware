<?php

namespace Drupal\sap_middleware\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\sap_middleware\SapMiddlewareService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the sap_sync_orders queue worker.
 *
 * @QueueWorker (
 *   id = "sap_sync_orders",
 *   title = @Translation("Send the order to the SAP API."),
 *   cron = {"time" = 60}
 * )
 */
class SyncOrders extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The queue worker logger channel.
   *
   * @var Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The sap middleware client.
   *
   * @var \Drupal\sap_middleware\SapMiddlewareService
   */
  protected $sapMiddlewareService;

  /**
   * Constructs a new SyncOrders object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, SapMiddlewareService $sap_middleware_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->sapMiddlewareService = $sap_middleware_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('queue'),
      $container->get('sap_middleware.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $this
        ->entityTypeManager
        ->getStorage('commerce_order')
        ->load($data['order_id']);

      // Make the SAP API call to send the order.
      $this->sapMiddlewareService->sendOrder($order);
    }
    // @todo Determine what to do on error.
    catch (\Exception $e) {
    }
  }

}
