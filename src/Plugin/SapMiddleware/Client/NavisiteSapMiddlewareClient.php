<?php

namespace Drupal\sap_middleware\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Elastica\Exception\Connection\GuzzleException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Provides the Navisite Middleware Client.
 *
 * @SapMiddlewareClient(
 *   id = "navisite_middleware_client",
 *   label = "Navisite Middleware Client",
 * )
 */
class NavisiteSapMiddlewareClient extends SapMiddlewareClientBase implements SapMiddlewareClientInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
      'test_api_oauth_url' => '',
      'live_api_oauth_url' => '',
      'test_api_sales_order_url' => '',
      'live_api_sales_order_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Return the Oauth authentication endpoint.
   *
   * @return string
   *   The url.
   */
  public function getOauthAuthenticationEndpoint() {
    return $this->getMode() === 'test' ? $this->configuration['test_api_oauth_url'] : $this->configuration['live_api_oauth_url'];
  }

  /**
   * Return the Sales order endpoint.
   *
   * @return string
   *   The url.
   */
  public function getSalesOrderEndpoint() {
    return $this->getMode() === 'test' ? $this->configuration['test_api_sales_order_url'] : $this->configuration['live_api_sales_order_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['authentication_details'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Navisite Oauth Authentication Details'),
    ];
    $form['authentication_details']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Set the username for authentication.'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
    ];
    $form['authentication_details']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Set the password for authentication.'),
      '#default_value' => $this->configuration['password'],
      '#required' => empty($this->configuration['password']),
    ];
    // Navisite API URLs are different for every client, so, it must be
    // collected.
    $form['authentication_details']['test_api_oauth_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test API Oauth URL'),
      '#description' => $this->t('Set the Test URL for Oauth authentication.'),
      '#default_value' => $this->configuration['test_api_oauth_url'],
      '#required' => TRUE,
    ];
    $form['authentication_details']['live_api_oauth_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Live API Oauth URL'),
      '#description' => $this->t('Set the Live URL for Oauth authentication.'),
      '#default_value' => $this->configuration['live_api_oauth_url'],
      '#required' => TRUE,
    ];
    $form['authentication_details']['test_api_sales_order_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test API Sales Order URL'),
      '#description' => $this->t('Set the Test URL for submitting sales orders.'),
      '#default_value' => $this->configuration['test_api_sales_order_url'],
      '#required' => TRUE,
    ];
    $form['authentication_details']['live_api_sales_order_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Live API Sales Order URL'),
      '#description' => $this->t('Set the Live URL for submitting sales orders.'),
      '#default_value' => $this->configuration['live_api_sales_order_url'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['username'] = $values['authentication_details']['username'];
      if (!empty($values['authentication_details']['password'])) {
        $this->configuration['password'] = $values['authentication_details']['password'];
      }
      $this->configuration['test_api_oauth_url'] = $values['authentication_details']['test_api_oauth_url'];
      $this->configuration['live_api_oauth_url'] = $values['authentication_details']['live_api_oauth_url'];
      $this->configuration['test_api_sales_order_url'] = $values['authentication_details']['test_api_sales_order_url'];
      $this->configuration['live_api_sales_order_url'] = $values['authentication_details']['live_api_sales_order_url'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    $config = $this->getConfiguration();

    try {
      $body = urldecode(UrlHelper::buildQuery([
        'grant_type' => 'client_credentials',
      ]));
      $auth = 'Basic ' . base64_encode($config['username'] . ':' . $config['password']);

      $oauth_endpoint = $this->getOauthAuthenticationEndpoint();
      $response = $this->client->post($oauth_endpoint, [
        'body' => $body,
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Authorization' => $auth,
        ],
      ]);
      if ($response->getStatusCode() != 200) {
        throw new InvalidArgumentException(sprintf('Invalid request. The response code was %s and response returned was %s', $response->getStatusCode(), json_decode($response->getBody())));
      }

      $data = json_decode($response->getBody());

      return $data->access_token;
    }
    catch (GuzzleException $e) {
      $this->logger->error(
        'An error was encountered while fetching an access token from the SAP Middleware Client. The error was: @error.', [
          '@error' => $e->getCode() . ' ' . $e->getMessage(),
        ]
      );
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getOrderPayload(OrderInterface $order): array {
    // Use an event subscriber that subscribes to the OrderPayloadEvent to
    // generate the site-specific payload.
    // @see \Drupal\sap_middleware\Event\OrderPayloadEvent
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sendOrder(array $payload, string $access_token) {
    try {
      $auth = 'Bearer ' . $access_token;

      $response = $this->client->post($this->getSalesOrderEndpoint(), [
        'body' => json_encode($payload),
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => $auth,
        ],
      ]);
      if ($response->getStatusCode() != 201) {
        throw new InvalidArgumentException(sprintf('Invalid request. The response code was %s and response returned was %s', $response->getStatusCode(), $response->getBody()));
      }

      // The body is in XML format, so convert it to an object.
      $data = $response->getBody()->getContents();
      $encode_data = json_encode(simplexml_load_string($data));
      $decoded_data = json_decode($encode_data, TRUE);

      return [
        'sap_order_id' => $decoded_data['SOrderHeader']['Vbeln'],
        'payload' => $payload,
      ];
    }
    catch (GuzzleException $e) {
      $this->logger->error(
        'An error was encountered while sending the order from the SAP Middleware Client. The error was: @error.', [
          '@error' => $e->getCode() . ' ' . $e->getMessage(),
        ]
      );
    }
  }

}
