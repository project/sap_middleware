<?php

namespace Drupal\sap_middleware\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the base class for SAP Middleware Clients.
 */
abstract class SapMiddlewareClientBase extends PluginBase implements SapMiddlewareClientInterface, ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $parentEntity;

  /**
   * Constructs a new SapMiddlewareClientBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $client
   *   Http client to connect with the configured SAP Middleware Client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $client, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->logger = $logger;

    if (array_key_exists('_entity', $configuration)) {
      $this->parentEntity = $configuration['_entity'];
      unset($configuration['_entity']);
    }
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('logger.factory')->get('sap_middleware'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    if (!empty($this->parentEntity)) {
      $this->_parentEntityId = $this->parentEntity->id();
      unset($this->parentEntity);
    }
    unset($this->profiles);

    return parent::__sleep();
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    parent::__wakeup();

    if (!empty($this->_parentEntityId)) {
      $sap_middleware_client_storage = $this->entityTypeManager->getStorage('sap_middleware_client');
      $this->parentEntity = $sap_middleware_client_storage->load($this->_parentEntityId);
      unset($this->_parentEntityId);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $modes = array_keys($this->getSupportedModes());

    return [
      'mode' => $modes ? reset($modes) : '',
      'order_types_to_sync' => [],
      'stores_to_sync' => [],
      'sync_orders_method' => 'cron_run',
      'sap_order_id_field' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $modes = $this->getSupportedModes();
    if (count($modes) > 1) {
      $form['mode'] = [
        '#type' => 'radios',
        '#title' => $this->t('Mode'),
        '#options' => $modes,
        '#default_value' => $this->configuration['mode'],
        '#required' => TRUE,
      ];
    }
    else {
      $mode_names = array_keys($modes);
      $form['mode'] = [
        '#type' => 'value',
        '#value' => reset($mode_names),
      ];
    }

    // Order setttings.
    $form['order_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Order Settings'),
    ];
    $form['order_settings']['order_types_to_sync'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Order types to sync'),
      '#description' => $this->t('Select the order types that should be synced to SAP'),
      '#target_type' => 'commerce_order_type',
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['order_types_to_sync'],
    ];
    $form['order_settings']['stores_to_sync'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Stores to sync'),
      '#description' => $this->t('Select the stores that should be synced to SAP'),
      '#target_type' => 'commerce_store',
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['stores_to_sync'],
    ];
    $form['order_settings']['sync_orders_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Sync orders method'),
      '#description' => $this->t('Select when orders should be synced to SAP. <strong>Note: </strong>At the moment only syncing via cron is enabled.'),
      '#default_value' => $this->configuration['sync_orders_method'],
      '#options' => [
        'cron_run' => $this->t('On Cron Run'),
        // @todo Figure out if syncing immediately is a good option.
        // 'order_submission' => $this->t('On Order Submission'),
      ],
      '#required' => TRUE,
    ];
    $options = [];
    $field_definitions = $this->entityFieldManager->getFieldStorageDefinitions('commerce_order');
    foreach ($field_definitions as $field_definition) {
      if ($field_definition->getType() != 'string') {
        continue;
      }

      $options[$field_definition->getName()] = $field_definition->getLabel();
    }
    $form['order_settings']['sap_order_id_field'] = [
      '#type' => 'select',
      '#title' => $this->t('SAP order ID field'),
      '#description' => $this->t('Select the field which will be used to save the ID returned from SAP after sync. This field will also be used to determine if the order has already been synced with SAP.'),
      '#options' => $options,
      '#default_value' => $this->configuration['sap_order_id_field'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['stores_to_sync'] = (array) $values['order_settings']['stores_to_sync'];
      $this->configuration['order_types_to_sync'] = (array) $values['order_settings']['order_types_to_sync'];
      $this->configuration['sync_orders_method'] = $values['order_settings']['sync_orders_method'];
      $this->configuration['sap_order_id_field'] = $values['order_settings']['sap_order_id_field'];
      $this->configuration['mode'] = $values['mode'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedModes() {
    return $this->pluginDefinition['modes'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMode() {
    return $this->configuration['mode'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncOrdersMethod() {
    return $this->configuration['sync_orders_method'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedStores() {
    return $this->configuration['stores_to_sync'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedOrderTypes() {
    return $this->configuration['order_types_to_sync'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSapOrderIdField() {
    return $this->configuration['sap_order_id_field'];
  }

  /**
   * {@inheritdoc}
   *
   * @see NavisiteSapMiddlewareClient::getAccessToken()
   */
  public function getAccessToken() {
    return '';
  }

  /**
   * {@inheritdoc}
   *
   * @see NavisiteSapMiddlewareClient::getOrderPayload()
   */
  public function getOrderPayload(OrderInterface $order): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @see NavisiteSapMiddlewareClient::sendOrder()
   */
  public function sendOrder(array $payload, string $access_token) {
    return [];
  }

}
