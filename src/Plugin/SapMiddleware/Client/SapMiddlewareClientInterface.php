<?php

namespace Drupal\sap_middleware\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the base interface for SAP Middleware clients.
 */
interface SapMiddlewareClientInterface extends ConfigurableInterface, DependentPluginInterface, PluginFormInterface, PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Gets the client label.
   *
   * @return string
   *   The client label.
   */
  public function getLabel();

  /**
   * Gets the mode in which the SAP Middleware Client is operating.
   *
   * @return string
   *   The machine name of the mode.
   */
  public function getMode();

  /**
   * Gets the supported modes.
   *
   * @return string[]
   *   The mode labels keyed by machine name.
   */
  public function getSupportedModes();

  /**
   * Gets the sync order method type name. ie. "cron_run".
   *
   * @return string
   *   The name of the sync method that was selected.
   */
  public function getSyncOrdersMethod();

  /**
   * Gets the supported stores.
   *
   * @return string[]
   *   An array of store IDs.
   */
  public function getSupportedStores();

  /**
   * Gets the supported order types.
   *
   * @return string[]
   *   An array of order type IDs.
   */
  public function getSupportedOrderTypes();

  /**
   * Gets the name of the SAP order ID field, if the order ID should be saved.
   *
   * @return string
   *   The field name of the field on the order entity.
   */
  public function getSapOrderIdField();

  /**
   * Authenticate with the middleware and return an access token.
   *
   * @return string
   *   The access token if the Oauth was successful.
   */
  public function getAccessToken();

  /**
   * Build the order payload to send to SAP.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return array
   *   The order payload.
   */
  public function getOrderPayload(OrderInterface $order): array;

  /**
   * Send order to SAP.
   *
   * @param array $payload
   *   The payload with the order details.
   * @param string $access_token
   *   The access token received after authentication.
   *
   * @return array
   *   An array with the SAP order ID and any other valuable information.
   *   return [
   *     'sap_order_id' => 'abc123',
   *     'order_details_from_sap' => [
   *         'order_id' => '123'
   *         'order_total' => 15.00,
   *         ...
   *     ],
   *   ];
   */
  public function sendOrder(array $payload, string $access_token);

}
