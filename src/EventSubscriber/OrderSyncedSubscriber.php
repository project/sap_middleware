<?php

namespace Drupal\sap_middleware\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\sap_middleware\Event\OrderSyncedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to the SAP order synced event.
 */
class OrderSyncedSubscriber implements EventSubscriberInterface {

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new OrderSyncedSubscriber object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get('sap_middleware');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderSyncedEvent::EVENT_NAME][] = ['onOrderSync'];
    return $events;
  }

  /**
   * Subscribes to the SAP order sync event.
   *
   * Updates the order with the order ID received from SAP after the sync.
   *
   * @param \Drupal\sap_middleware\Event\OrderSyncedEvent $event
   *   The order sync event.
   */
  public function onOrderSync(OrderSyncedEvent $event) {
    // Update the order with the SAP order id.
    $order = $event->getOrder();
    $client_plugin = $event->getClient()->getPlugin();
    $sap_order_id = $event->getResult()['sap_order_id'];

    $sap_order_id_field = $client_plugin->getSapOrderIdField();
    if ($order->hasField($sap_order_id_field)) {
      $order->set($sap_order_id_field, $sap_order_id);
      $order->save();
    }
  }

}
