<?php

namespace Drupal\sap_middleware\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface;

/**
 * Event that is fired when an order is synced to SAP.
 */
class OrderSyncedEvent extends Event {

  const EVENT_NAME = 'sap_middleware_order_synced_event';

  /**
   * The SAP Middleware client entity that was used to sync the order.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $client;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The order payload.
   *
   * @var array
   */
  protected $payload;

  /**
   * The result returned by the client after syncing the order with SAP.
   *
   * @var array
   */
  protected $result;

  /**
   * Constructs the OrderSyncedEvent object.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   The SAP Middleware client entity.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity was synced.
   * @param array $payload
   *   The order payload.
   * @param array $result
   *   The result returned by the client after syncing the order with SAP.
   */
  public function __construct(SapMiddlewareClientInterface $client, OrderInterface $order, array $payload, array $result) {
    $this->client = $client;
    $this->order = $order;
    $this->payload = $payload;
    $this->result = $result;
  }

  /**
   * Gets the client.
   *
   * @return \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   *   The SAP Middleware client entity.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Gets the order payload.
   *
   * @return array
   *   The order payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Gets the result returned by the client after syncing the order with SAP.
   *
   * @return array
   *   The result of the sync.
   */
  public function getResult() {
    return $this->result;
  }

}
