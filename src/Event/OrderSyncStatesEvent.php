<?php

namespace Drupal\sap_middleware\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface;

/**
 * Event that is fired when orders are about to be fetched for syncing.
 *
 * Allows modules to alter the order states that should be included in the sync.
 */
class OrderSyncStatesEvent extends Event {

  const EVENT_NAME = 'sap_middleware_order_sync_states_event';

  /**
   * The SAP Middleware client entity that is being used to sync the order.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $client;

  /**
   * The order states.
   *
   * @var array
   */
  protected $states;

  /**
   * Constructs the OrderSyncStatesEvent object.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   The SAP Middleware client entity.
   * @param array $states
   *   The order states.
   */
  public function __construct(SapMiddlewareClientInterface $client, array $states) {
    $this->client = $client;
    $this->states = $states;
  }

  /**
   * Gets the client.
   *
   * @return \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   *   The SAP Middleware client entity.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Gets the order states.
   *
   * @return array
   *   The order states.
   */
  public function getStates() {
    return $this->states;
  }

  /**
   * Sets the order states.
   *
   * @param array $states
   *   The order states.
   *
   * @return $this
   */
  public function setStates(array $states) {
    $this->states = $states;
    return $this;
  }

}
