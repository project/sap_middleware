<?php

namespace Drupal\sap_middleware\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface;

/**
 * Event that is fired when an order payload is fetched to sent to SAP.
 */
class OrderPayloadEvent extends Event {

  const EVENT_NAME = 'sap_middleware_order_payload_event';

  /**
   * The SAP Middleware client entity that is being used to sync the order.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $client;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The order payload.
   *
   * @var array
   */
  protected $payload;

  /**
   * Constructs the OrderPayloadEvent object.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   The SAP Middleware client entity.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity was synced.
   * @param array $payload
   *   The order payload.
   */
  public function __construct(SapMiddlewareClientInterface $client, OrderInterface $order, array $payload) {
    $this->client = $client;
    $this->order = $order;
    $this->payload = $payload;
  }

  /**
   * Gets the client.
   *
   * @return \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   *   The SAP Middleware client entity.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Gets the order payload.
   *
   * @return array
   *   The order payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Sets the order payload.
   *
   * @param array $payload
   *   The payload.
   *
   * @return $this
   */
  public function setPayload(array $payload) {
    $this->payload = $payload;
    return $this;
  }

}
