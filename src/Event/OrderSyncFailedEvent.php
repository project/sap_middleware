<?php

namespace Drupal\sap_middleware\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface;

/**
 * Event that is fired when an order fails syncing to SAP.
 */
class OrderSyncFailedEvent extends Event {

  const EVENT_NAME = 'sap_middleware_order_sync_failed_event';

  /**
   * The SAP Middleware client entity that was used to sync the order.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $client;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The order payload.
   *
   * @var array
   */
  protected $payload;

  /**
   * The exception that was received.
   *
   * @var \Exception
   */
  protected $exception;

  /**
   * Constructs the OrderSyncFailedEvent object.
   *
   * @param \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface $client
   *   The SAP Middleware client entity.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity was synced.
   * @param array $payload
   *   The order payload.
   * @param \Exception $exception
   *   The exception that was received.
   */
  public function __construct(SapMiddlewareClientInterface $client, OrderInterface $order, array $payload, \Exception $exception) {
    $this->client = $client;
    $this->order = $order;
    $this->payload = $payload;
    $this->exception = $exception;
  }

  /**
   * Gets the client.
   *
   * @return \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   *   The SAP Middleware client entity.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Gets the order payload.
   *
   * @return array
   *   The order payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Gets the exception that was received.
   *
   * @return \Exception
   *   The exception that was received.
   */
  public function getException() {
    return $this->exception;
  }

}
