# Introduction

Manages SAP Middleware Integration which allows for the syncing of orders to SAP.

Adds the following features:

- Creates a new plugin type for allowing modules to create their own SAP Middleware
Client
- Creates a default SAP client for integrating the Navisite Middleware Client
with SAP
- Syncs orders to SAP using the middleware of choice and updates the order with
the SAP order ID that is returned
  - Field storage configuration for `field_sap_order_id` exists in the optional
configs directory which can be imported and used to create the field on all
Commerce Order types which can then be selected on the client as the field to
use to save the order ID returned from SAP

# Requirements

None.

# Installation

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

# Configuration
1. Add the `field_sap_order_id` field on all Commerce Order types
  - This can be omitted if you wish to use a different field for storing the SAP
order ID
2. Go to `/admin/config/services/sap_middleware_settings` and select the SAP Middleware
Client that should be used to sync to SAP
2. Select the order types that should be synced to SAP
3. Select the stores that should be synced to SAP
4. Select the order sync method
5. Select `field_sap_order_id` as the field on the order entity that the SAP
order ID should be saved to
  - Alternately, you can select another field to do this

# Maintainers

Acro Media Inc.
