# Introduction

Enables reporting for the SAP Middleware module.

Adds the following features:

- Two new additional fields will be created on each order type if reporting is enabled on the client
plugin
  - `field_sap_sync_failed` - TRUE if the sync failed
  - `field_sap_sync_details` - A serialized array field that contains data about
the sync
    - For successful syncs, will have the last synced date and time, the payload
    - For failed syncs, will have the last synced date and time, a count of the
number of times the sync was retried, the error code that was received, the
error message that was received, the payload
- If reporting is unchecked after being enabled and there are no other enabled
clients that have reporting enabled, the fields will be deleted from the order
type. This can cause data that was stored on these fields to be lost.
- Three views will be created
  - A view of all the orders that were synced to SAP
  - A view of all the orders that are pending syncing to SAP
  - A view of all the orders that have failed syncing to SAP

# Requirements

None.

# Installation

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

# Configuration

The SAP Orders view is installed under the assumption that a `field_sap_order_id`
field was created for all order types and selected as the SAP order ID field for
the enabled SAP Middleware Client.

If a different field other than `field_sap_order_id` is used as the field to
save the SAP order ID field on the order entity, then, the SAP Orders view needs
to be re-configured:

- Go to `admin/structure/views/view/sap_orders`
- Click on "Synced Orders" display
  - Under the "Filter Criteria" remove the `Order: SAP Middleware order ID field (not empty)` field
  - Re-add the field again and select "Is not empty (NOT NULL)" as the Operator
- Click on "Orders Pending Sync" display
  - Under the "Filter Criteria" remove the `Order: SAP Middleware order ID field (empty)` field
  - Re-add the field again and select "Is empty (NULL)" as the Operator
- Click on "Sync Failed Orders" display
  - Under the "Filter Criteria" remove the `Order: SAP Middleware order ID field (empty)` field
  - Re-add the field again and select "Is empty (NULL)" as the Operator
- Save the view

# Maintainers

Acro Media Inc.
