<?php

namespace Drupal\sap_middleware_reports\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\sap_middleware\Event\OrderSyncFailedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to the SAP order sync failed event.
 */
class OrderSyncFailedSubscriber implements EventSubscriberInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new OrderSyncFailedSubscriber object.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderSyncFailedEvent::EVENT_NAME][] = ['onOrderSyncFailure'];
    return $events;
  }

  /**
   * Subscribes to the SAP order sync failed event.
   *
   * Updates the new order sync fields with the sync failure details.
   *
   * @param \Drupal\sap_middleware\Event\OrderSyncFailedEvent $event
   *   The order sync event.
   */
  public function onOrderSyncFailure(OrderSyncFailedEvent $event) {
    $client = $event->getClient();
    if (!$client->getThirdPartySetting('sap_middleware_reports', 'enable_reporting')) {
      return;
    }

    // Save the details in the sap sync fields.
    $payload = $event->getPayload();
    $order = $event->getOrder();
    $exception = $event->getException();

    // If the order didn't sync, then, this field would be TRUE.
    $order->set('field_sap_sync_failed', TRUE);
    // Add the sync failure details.
    $failed_count = 1;
    // If this order has multiple failures, then, upp the failed count.
    if (!$order->get('field_sap_sync_details')->isEmpty()) {
      $failed_sync_details = Json::decode($order->get('field_sap_sync_details')->first()->getValue()['value']);
      if ($failed_sync_details['sync_failed_count']) {
        $failed_count = $failed_sync_details['sync_failed_count'] + 1;
      }
    }
    $order->set('field_sap_sync_details', [
      'value' => Json::encode([
        'payload' => (array) $payload,
        'exception' => (array) $exception,
        'sync_failed_count' => $failed_count,
        'synced_time' => $this->time->getRequestTime(),
      ]),
    ]);
    $order->save();
  }

}
