<?php

namespace Drupal\sap_middleware_reports\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\sap_middleware\Event\OrderSyncedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to the SAP order synced event.
 */
class OrderSyncedSubscriber implements EventSubscriberInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new OrderSyncedSubscriber object.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderSyncedEvent::EVENT_NAME][] = ['onOrderSync'];
    return $events;
  }

  /**
   * Subscribes to the SAP order sync event.
   *
   * Updates the new order sync fields with the sync details.
   *
   * @param \Drupal\sap_middleware\Event\OrderSyncedEvent $event
   *   The order sync event.
   */
  public function onOrderSync(OrderSyncedEvent $event) {
    $client = $event->getClient();
    if (!$client->getThirdPartySetting('sap_middleware_reports', 'enable_reporting')) {
      return;
    }

    // Save the details in the sap sync fields.
    $sap_order_id = $event->getResult()['sap_order_id'];
    $payload = $event->getPayload();
    $order = $event->getOrder();

    // If the order synced, then, this field would be FALSE.
    $order->set('field_sap_sync_failed', FALSE);
    // Add the sync details.
    $order->set('field_sap_sync_details', [
      'value' => Json::encode([
        'payload' => (array) $payload,
        'sap_order_id' => $sap_order_id,
        'synced_time' => $this->time->getRequestTime(),
      ]),
    ]);
    $order->save();
  }

}
