<?php

namespace Drupal\sap_middleware_reports\Plugin\views\field;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides a field handler that renders the SAP sync details field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("sap_middleware_field_sap_sync_details")
 */
class SyncDetails extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['key'] = ['default' => NULL];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['key'] = [
      '#title' => $this->t('The key to output. ie. payload'),
      '#type' => 'textfield',
      '#default_value' => $this->options['key'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    if (!$value) {
      return [
        '#markup' => '',
      ];
    }

    $render_key = $this->options['key'];
    if (!$render_key) {
      return [
        '#markup' => '<pre>' . $value . '</pre>',
      ];
    }
    else {
      $values = Json::decode(unserialize($value, ['allowed_classes' => FALSE]), TRUE);
      if (isset($values[$render_key])) {
        $item = $values[$render_key];
        $markup = is_array($item) || is_object($item) ? '<pre>' . Json::encode($item) . '</pre>' : (string) $item;
        return [
          '#markup' => $markup,
        ];
      }
    }

    return [
      '#markup' => '',
    ];
  }

}
