<?php

/**
 * @file
 * Contains Views integration for the SAP Middleware Reports module.
 */

/**
 * Implements hook_views_data_alter().
 *
 * Add a field definition for the SAP Order ID field.
 */
function sap_middleware_reports_views_data_alter(array &$data) {
  // As the SAP Order ID field name is dynamic and not know beforehand, fetch it
  // from the enabled client and add the views definition for the field from the
  // existing data, if a definition for the field exists for commerce_order.
  /** @var \Drupal\sap_middleware\SapMiddlewareService $sap_middleware_service */
  $sap_middleware_service = \Drupal::service('sap_middleware.service');
  $client = $sap_middleware_service->getClient();
  if (!$client) {
    return;
  }

  $sap_order_id_field = $client->getPlugin()->getSapOrderIdField();
  if (isset($data["commerce_order__$sap_order_id_field"])) {
    $data['commerce_order__sap_middleware_sap_order_id_field'] = $data["commerce_order__$sap_order_id_field"];
  }
  else {
    foreach ($data['commerce_order'] as $field) {
      if (isset($field[$sap_order_id_field])) {
        $data['commerce_order__sap_middleware_sap_order_id_field'] = $field[$sap_order_id_field];
      }
    }
  }

  // Change the title and help so that it is recognizable form the original
  // field definition.
  $title = t('SAP Middleware order ID field');
  $description = t('The SAP order ID field selected in the enabled SAP Middleware client.');
  if (isset($data['commerce_order__sap_middleware_sap_order_id_field']['title'])) {
    $data['commerce_order__sap_middleware_sap_order_id_field']['title'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field']['title short'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field']['help'] = $description;
  }
  if (isset($data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field]['title'])) {
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field]['title'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field]['title short'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field]['help'] = $description;
  }
  if (isset($data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field . '_value']['title'])) {
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field . '_value']['title'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field . '_value']['title short'] = $title;
    $data['commerce_order__sap_middleware_sap_order_id_field'][$sap_order_id_field . '_value']['help'] = $description;
  }

  // Use a custom field plugin to render the `field_sync_details` field.
  $data['commerce_order__field_sap_sync_details']['field_sap_sync_details']['field']['id'] = 'sap_middleware_field_sap_sync_details';
}
