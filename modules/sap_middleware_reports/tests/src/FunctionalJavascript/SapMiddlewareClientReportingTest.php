<?php

namespace Drupal\Tests\sap_middleware_reports\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Test adding and configuring an SAP Middleware client with reporting enabled.
 *
 * @group sap_middleware
 */
class SapMiddlewareClientReportingTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'sap_middleware',
    'sap_middleware_reports',
  ];

  /**
   * An admin user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer site configuration'], NULL, TRUE);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test that sync fields are created when reporting is enabled on the client.
   *
   * @covers ::sap_middleware_reports_client_form_submit()
   */
  public function testFieldsCreatedOnReportingEnabled() {
    // Enable reporting.
    $this->drupalGet(Url::fromRoute('entity.sap_middleware_client.add_form'));
    $this->assertSession()->pageTextContains('Add SAP Middleware Client');
    $this->assertSession()->fieldExists('Navisite Middleware Client');
    $page = $this->getSession()->getPage();
    $page->fillField('label', 'Navisite Client');
    $this->assertSession()->waitForText('navisite_client');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][username]', 'testuser');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][password]', 'testpassword');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][test_api_oauth_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][live_api_oauth_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][test_api_sales_order_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][live_api_sales_order_url]', 'http://example.com');
    $page->checkField('Enable Reporting');
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('Saved the Navisite Client SAP Middleware Client.');
    $this->assertSession()->pageTextContains('The SAP reporting fields have been successfully created on all order types and the SAP reporting views have been enabled as well.');
    $this->clickLink('Edit');
    $this->assertSession()->fieldValueEquals('Username', 'testuser');
    $this->assertSession()->fieldValueEquals('Live API Oauth URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Live API Sales Order URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Enable Reporting', '1');
    $this->assertSession()->fieldValueEquals('Enabled', '1');

    // Assert that the fields were created.
    $this->assertSapSyncFields(TRUE);
    // Confirm the views are displaying now.
    $this->drupalGet(Url::fromRoute('entity.commerce_order.collection'));
    $this->assertSession()->linkExists('Synced Orders');
    $this->assertSession()->linkExists('Orders Pending Sync');
    $this->assertSession()->linkExists('Sync Failed Orders');

    // Now disable reporting and ensure the fields have been deleted.
    $this->drupalGet(Url::fromRoute('entity.sap_middleware_client.collection'));
    $this->clickLink('Edit');
    $page->uncheckField('Enable Reporting');
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('The SAP reporting fields have been successfully removed from all order types and the SAP reporting views have been disabled as well.');
    $this->assertSapSyncFields(FALSE);
    // Confirm the views are NOT displaying now.
    $this->drupalGet(Url::fromRoute('entity.commerce_order.collection'));
    $this->assertSession()->linkNotExists('Synced Orders');
    $this->assertSession()->linkNotExists('Orders Pending Sync');
    $this->assertSession()->linkNotExists('Sync Failed Orders');
  }

  /**
   * Assert SAP sync fields exist/don't exist depending on parameter passed.
   *
   * @param bool $exists
   *   Assert that the fields were created.
   */
  private function assertSapSyncFields(bool $exists) {
    // Check that the fields are created on the order type.
    $order_types = \Drupal::entityTypeManager()->getStorage('commerce_order')->loadMultiple();
    foreach ($order_types as $order_type) {
      $field_definitions = sap_middleware_reports_build_reporting_field_definitions($order_type->id());
      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      foreach ($field_definitions as $field_name => $field_definition) {
        $field = FieldConfig::loadByName('commerce_order', $order_type->id(), $field_name);
        if ($exists) {
          $this->assertNotEmpty($field);
        }
        else {
          $this->assertEmpty($field);
        }
      }
    }
  }

}
