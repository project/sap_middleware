<?php

namespace Drupal\Tests\sap_middleware_reports\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\sap_middleware\Entity\SapMiddlewareClient;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Tests reporting on order syncing and order sync failure.
 *
 * @coversDefaultClass \Drupal\sap_middleware\Plugin\QueueWorker\SyncOrders
 * @group sap_middleware
 */
class OrderSyncReportingTest extends CommerceBrowserTestBase {
  use CronRunTrait;

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapMiddlewareClient;

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapErrorMiddlewareClient;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_price',
    'options',
    'commerce_product',
    'commerce_order',
    'sap_middleware',
    'sap_middleware_reports',
    'sap_middleware_client_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'access commerce_order overview',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldConfig::create([
      'field_name' => 'field_sap_order_id',
      'entity_type' => 'commerce_order',
      'bundle' => 'default',
      'label' => 'SAP Order ID',
    ])->save();

    // Create an SAP Middleware Client entity.
    $this->sapMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_middleware_client',
      'label' => 'Example Client',
      'plugin' => 'example_middleware_client',
      'status' => TRUE,
      'configuration' => [
        'mode' => 'test',
        'sync_orders_method' => 'cron_run',
        'stores_to_sync' => [1],
        'order_types_to_sync' => ['default'],
        'sap_order_id_field' => 'field_sap_order_id',
      ],
    ]);
    $this->sapMiddlewareClient->setThirdPartySetting('sap_middleware_reports', 'enable_reporting', TRUE);
    $this->sapMiddlewareClient->save();
    // Create an SAP Middleware Client entity that will throw an error.
    $this->sapErrorMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_error_middleware_client',
      'label' => 'Example Error Client',
      'plugin' => 'example_error_middleware_client',
      'status' => FALSE,
      'configuration' => [
        'mode' => 'test',
        'sync_orders_method' => 'cron_run',
        'stores_to_sync' => [1],
        'order_types_to_sync' => ['default'],
        'sap_order_id_field' => 'field_sap_order_id',
      ],
    ]);
    $this->sapErrorMiddlewareClient->setThirdPartySetting('sap_middleware_reports', 'enable_reporting', TRUE);
    $this->sapErrorMiddlewareClient->save();

    // Create the reporting fields on the order types.
    sap_middleware_reports_create_reporting_fields();
    // Enable the SAP Orders view.
    \Drupal::entityTypeManager()->getStorage('view')
      ->load('sap_orders')
      ->setStatus(TRUE)
      ->save();
    // Flush caches so the views menus can be updated.
    drupal_flush_all_caches();

    // Creates sample order.
    $this->createOrder();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test reporting succeeded on order syncing success.
   *
   * @covers \Drupal\sap_middleware_reports\EventSubscriber\OrderSyncedSubscriber
   */
  public function testOrderReportingOnSyncing() {
    // Test there is only 1 order in the db.
    $orders = Order::loadMultiple();
    $this->assertCount(1, $orders);
    $order = end($orders);
    // Test the sync fields are empty.
    $sync_fields = [
      'field_sap_order_id',
      'field_sap_sync_failed',
      'field_sap_sync_details',
    ];
    foreach ($sync_fields as $field) {
      $this->assertEmpty($order->get($field)->value);
    }

    // Run cron, and ensure the order has synced.
    $this->cronRun();

    // Ensure order has synced.
    $order = $this->reloadEntity($order);

    // Confirm the sync fields have the expected values in them now.
    // Sync failed field should still be FALSE.
    $this->assertNotEmpty($order->get('field_sap_order_id')->value);
    $this->assertFalse((bool) $order->get('field_sap_sync_failed')->value);
    $this->assertNotEmpty($order->get('field_sap_sync_details')->value);
    $sap_sync_details = Json::decode($order->get('field_sap_sync_details')->first()->getValue()['value']);
    $this->assertArrayHasKey('payload', $sap_sync_details);
    $this->assertArrayHasKey('sap_order_id', $sap_sync_details);
    $this->assertArrayHasKey('synced_time', $sap_sync_details);

    // Confirm the order displays in the synced orders list.
    $this->drupalGet(Url::fromRoute('view.sap_orders.synced_orders'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($order->id());
    $this->assertSession()->pageTextContains($order->id() . '10001');
    $this->assertSession()->pageTextContains('payload');
    $this->assertSession()->pageTextContains('sap_order_id');
    $this->assertSession()->pageTextContains('synced_time');
  }

  /**
   * Test reporting succeeded on order sync failure.
   *
   * @covers \Drupal\sap_middleware_reports\EventSubscriber\OrderSyncFailedSubscriber
   */
  public function testOrderReportingOnSyncFailure() {
    // Enable the faulty client that throws an error and disable the other.
    $this->sapErrorMiddlewareClient->enable()->save();
    $this->sapMiddlewareClient->disable()->save();
    // Fetch the order.
    $orders = Order::loadMultiple();
    $order = end($orders);

    // Test the sync fields are empty.
    $sync_fields = [
      'field_sap_order_id',
      'field_sap_sync_failed',
      'field_sap_sync_details',
    ];
    foreach ($sync_fields as $field) {
      $this->assertEmpty($order->get($field)->value);
    }

    // Run cron, and ensure the order has synced.
    $this->cronRun();

    // Ensure order has NOT synced.
    $order = $this->reloadEntity($order);

    // Confirm the sync fields have the expected values in them now.
    // Sync failed field should be TRUE now.
    $this->assertEmpty($order->get('field_sap_order_id')->value);
    $this->assertTrue((bool) $order->get('field_sap_sync_failed')->value);
    $this->assertNotEmpty($order->get('field_sap_sync_details')->value);
    $sap_sync_details = Json::decode($order->get('field_sap_sync_details')->first()->getValue()['value']);
    $this->assertArrayHasKey('payload', $sap_sync_details);
    $this->assertArrayHasKey('exception', $sap_sync_details);
    $this->assertArrayHasKey('sync_failed_count', $sap_sync_details);
    $this->assertArrayHasKey('synced_time', $sap_sync_details);

    // Confirm the order displays in the sync failed orders list.
    $this->drupalGet(Url::fromRoute('view.sap_orders.sync_failed_orders'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($order->id());
    $this->assertSession()->pageTextContains('payload');
    $this->assertSession()->pageTextContains('exception');
    $this->assertSession()->pageTextContains('synced_time');
    $this->assertSession()->pageTextContains('The specified access token is invalid');
  }

  /**
   * Creates an order entity.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  protected function createOrder() {
    $order_item = $this->createEntity('commerce_order_item', [
      'type' => 'default',
      'unit_price' => [
        'number' => '999',
        'currency_code' => 'USD',
      ],
    ]);
    $time = strtotime('now');
    $order = $this->createEntity('commerce_order', [
      'type' => 'default',
      'mail' => $this->loggedInUser->getEmail(),
      'order_items' => [$order_item],
      'uid' => $this->loggedInUser,
      'store_id' => $this->store,
      'state' => 'completed',
      'placed' => $time,
      'completed' => $time,
    ]);
    $order->save();

    return $this->reloadEntity($order);
  }

}
