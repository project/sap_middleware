<?php

namespace Drupal\Tests\sap_middleware\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Test adding and configuring an SAP Middleware client.
 *
 * @group sap_middleware
 */
class SapMiddlewareClientTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'sap_middleware',
  ];

  /**
   * An admin user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer site configuration'], NULL, TRUE);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test the collection page that lists all the clients.
   */
  public function testListBuilderPage() {
    $this->drupalGet(Url::fromRoute('entity.sap_middleware_client.collection'));
    $this->assertSession()->pageTextContains('There are no SAP Middleware Clients yet.');
    $this->assertSession()->linkExists('Add SAP Middleware Client');
  }

  /**
   * Test adding a new client.
   */
  public function testAddingNewClient() {
    $this->drupalGet(Url::fromRoute('entity.sap_middleware_client.add_form'));
    $this->assertSession()->pageTextContains('Add SAP Middleware Client');
    $fields = [
      'Name',
      'id',
      'Enabled',
    ];
    foreach ($fields as $field) {
      $this->assertSession()->fieldExists($field);
    }
    $this->assertSession()->pageTextContains('Plugin');
    $this->assertSession()->pageTextContains('Mode');
  }

  /**
   * Test Navisite client.
   */
  public function testNavisiteClient() {
    $this->drupalGet(Url::fromRoute('entity.sap_middleware_client.add_form'));
    $this->assertSession()->pageTextContains('Add SAP Middleware Client');
    $this->assertSession()->fieldExists('Navisite Middleware Client');
    $page = $this->getSession()->getPage();
    $page->fillField('label', 'Navisite Client');
    $this->assertSession()->waitForText('navisite_client');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][username]', 'testuser');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][password]', 'testpassword');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][test_api_oauth_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][live_api_oauth_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][test_api_sales_order_url]', 'http://example.com');
    $page->fillField('configuration[navisite_middleware_client][authentication_details][live_api_sales_order_url]', 'http://example.com');
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('Saved the Navisite Client SAP Middleware Client.');
    $this->clickLink('Edit');
    $this->assertSession()->fieldValueEquals('Username', 'testuser');
    $this->assertSession()->fieldValueEquals('Live API Oauth URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Live API Sales Order URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Enabled', '1');
  }

}
