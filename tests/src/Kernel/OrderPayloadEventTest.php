<?php

namespace Drupal\Tests\sap_middleware\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\sap_middleware\Entity\SapMiddlewareClient;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the OrderPayloadEvent class.
 *
 * @coversDefaultClass \Drupal\sap_middleware\Event\OrderPayloadEvent
 * @group sap_middleware
 */
class OrderPayloadEventTest extends OrderKernelTestBase {

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapMiddlewareClient;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_log',
    'sap_middleware',
    'sap_middleware_client_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['sap_middleware']);

    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface sapMiddlewareClient */
    $this->sapMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_middleware_client',
      'label' => 'Example Client',
      'plugin' => 'example_middleware_client',
      'status' => TRUE,
    ]);
    $this->sapMiddlewareClient->save();
  }

  /**
   * Tests that the order payload is altered if an event subscriber exists.
   *
   * @covers Drupal\sap_middleware_client_example\EventSubscriber\OrderPayloadSubscriber
   */
  public function testOrderPayload() {
    $user = $this->createUser();
    $order = Order::create([
      'type' => 'default',
      'state' => 'completed',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'order_number' => '1',
      'store_id' => $this->store->id(),
    ]);
    $order->save();

    // Assert the payload before the event is fired.
    $payload = $this->sapMiddlewareClient->getPlugin()->getOrderPayload($order);
    $this->assertCount(1, $payload);
    $this->assertArrayHasKey('order', $payload);
    $this->assertArrayNotHasKey('sap_customer_number', $payload);

    // Assert the payload after the event is fired.
    $sap_middleware_service = $this->container->get('sap_middleware.service');
    $order_payload = $sap_middleware_service->getOrderPayload($this->sapMiddlewareClient, $order);
    $this->assertCount(2, $order_payload);
    $this->assertArrayHasKey('order', $order_payload);
    $this->assertArrayHasKey('sap_customer_number', $order_payload);
  }

}
