<?php

namespace Drupal\Tests\sap_middleware\Kernel;

use Drupal\sap_middleware\Entity\SapMiddlewareClient;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the OrderSyncStatesEvent class.
 *
 * @coversDefaultClass \Drupal\sap_middleware\Event\OrderSyncStatesEvent
 * @group sap_middleware
 */
class OrderSyncStatesEventTest extends OrderKernelTestBase {

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapMiddlewareClient;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_log',
    'sap_middleware',
    'sap_middleware_client_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['sap_middleware']);

    /** @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface sapMiddlewareClient */
    $this->sapMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_middleware_client',
      'label' => 'Example Client',
      'plugin' => 'example_middleware_client',
      'status' => TRUE,
    ]);
    $this->sapMiddlewareClient->save();
  }

  /**
   * Tests that the order sync states is altered if an event subscriber exists.
   *
   * @covers Drupal\sap_middleware_client_example\EventSubscriber\OrderSyncStatesSubscriber
   */
  public function testSyncStates() {
    // Assert the order states after the event is fired.
    $sap_middleware_service = $this->container->get('sap_middleware.service');
    $order_states = $sap_middleware_service->getOrderStatesToSync($this->sapMiddlewareClient);
    $this->assertCount(2, $order_states);
    $this->assertEquals('completed', $order_states[0]);
    $this->assertEquals('pending', $order_states[1]);
  }

}
