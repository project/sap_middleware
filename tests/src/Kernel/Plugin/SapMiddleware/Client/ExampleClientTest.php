<?php

namespace Drupal\Tests\sap_middleware\Kernel\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\Order;
use Drupal\sap_middleware\Entity\SapMiddlewareClient;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the example client.
 *
 * @coversDefaultClass \Drupal\sap_middleware_client_example\Plugin\SapMiddleware\Client\ExampleClient
 * @group sap_middleware
 */
class ExampleClientTest extends OrderKernelTestBase {

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapMiddlewareClient;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sap_middleware',
    'sap_middleware_client_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['sap_middleware']);

    $this->sapMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_middleware_client',
      'label' => 'Example Client',
      'plugin' => 'example_middleware_client',
      'status' => TRUE,
    ]);
    $this->sapMiddlewareClient->save();
  }

  /**
   * @covers ::getAccessToken
   * @covers ::getOrderPayload
   * @covers ::sendOrder
   */
  public function testPlugin() {
    $plugin = $this->sapMiddlewareClient->getPlugin();
    $access_token = $plugin->getAccessToken();
    $this->assertEquals($access_token, $plugin->getAccessToken());
    $order = Order::create([
      'type' => 'default',
      'store_id' => 1,
      'state' => 'completed',
    ]);
    $order->save();
    $this->assertEquals(['order' => $order], $plugin->getOrderPayload($order));
    $this->assertEquals(
      ['sap_order_id' => $order->id() . '10001'],
      $plugin->sendOrder(['order' => $order], $access_token)
    );
  }

}
