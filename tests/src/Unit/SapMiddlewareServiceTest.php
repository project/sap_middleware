<?php

namespace Drupal\Tests\sap_middleware\Unit;

use Drupal\commerce_log\Entity\LogInterface;
use Drupal\commerce_log\LogStorage;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientInterface;
use Drupal\sap_middleware\SapMiddlewareService;
use Drupal\sap_middleware\Entity\SapMiddlewareClientInterface as SapMiddlewareClientEntityInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @coversDefaultClass \Drupal\sap_middleware\SapMiddlewareService
 * @group sap_middleware
 */
class SapMiddlewareServiceTest extends UnitTestCase {

  /**
   * The SAP Middleware service.
   *
   * @var \Drupal\sap_middleware\SapMiddlewareService
   */
  protected $sapMiddlewareService;

  /**
   * The mocked order entity.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The mock logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The mock Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mock event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The mock queue factory object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create the SAP Middleware service object.
    $this->createSapMiddlewareService();
  }

  /**
   * @covers ::getClient
   *
   * @dataProvider providerTestGetClient
   */
  public function testGetClient($input, $expected) {
    $sap_middleware_client = $this->sapMiddlewareService->getClient($input);
    $this->assertNotNull($sap_middleware_client);
    $this->assertEquals($expected, $sap_middleware_client->id());
  }

  /**
   * Provides data to self::testGetClient().
   */
  public function providerTestGetClient() {
    return [
        [NULL, 'example_client'],
        ['another_client', 'another_client'],
    ];
  }

  /**
   * @covers ::getOrderStatesToSync
   */
  public function testGetOrderStatesToSync() {
    $sap_middleware_example_client_plugin = $this->prophesize(SapMiddlewareClientInterface::class);
    $sap_middleware_example_client_entity = $this->prophesize(SapMiddlewareClientEntityInterface::class);
    $sap_middleware_example_client_entity->getPlugin()->willReturn($sap_middleware_example_client_plugin->reveal());
    $order_states = $this->sapMiddlewareService->getOrderStatesToSync($sap_middleware_example_client_entity->reveal());
    $this->assertEquals(['completed'], $order_states);
  }

  /**
   * @covers ::getOrderPayload
   *
   * @dataProvider providerTestGetOrderPayload
   */
  public function testGetOrderPayload($args) {
    $sap_middleware_example_client_plugin = $this->prophesize(SapMiddlewareClientInterface::class);
    $sap_middleware_example_client_plugin->getOrderPayload($this->order)->willReturn($args);
    $sap_middleware_example_client_entity = $this->prophesize(SapMiddlewareClientEntityInterface::class);
    $sap_middleware_example_client_entity->getPlugin()->willReturn($sap_middleware_example_client_plugin->reveal());
    $order_payload = $this->sapMiddlewareService->getOrderPayload($sap_middleware_example_client_entity->reveal(), $this->order);
    $this->assertEquals($args, $order_payload);
  }

  /**
   * Provides data to self::testGetOrderPayload().
   */
  public function providerTestGetOrderPayload() {
    return [
      [['order' => ['order_id' => 1]]],
      [['order' => ['order_id' => 1], 'a_field' => 111]],
      [['order' => ['order_id' => 1], 'another_field' => 123]],
      [[]],
    ];
  }

  /**
   * @covers ::syncOrders
   */
  public function testSyncOrders() {
    $this->sapMiddlewareService->syncOrders();
  }

  /**
   * @covers ::getAccessToken
   *
   * @dataProvider providerTestGetAccessToken
   */
  public function testGetAccessToken($input, $expected) {
    $access_token = $this->sapMiddlewareService->getAccessToken($input);
    $this->assertNotNull($access_token);
    $this->assertEquals($expected, $access_token);
  }

  /**
   * Provides data to self::testGetAccessToken().
   */
  public function providerTestGetAccessToken() {
    return [
      [NULL, '123456'],
      ['another_client', '789101'],
    ];
  }

  /**
   * @covers ::sendOrder
   *
   * @dataProvider providerTestSendOrder
   */
  public function testSendOrder($client_id, $access_token, $expected) {
    if ($client_id == 'exception_client') {
      $this->expectExceptionMessage('There was an error while syncing the order to SAP. The error code was: 0 and the error message was: Failed to connect to server.');
    }
    if ($client_id == 'a_non_existent_client') {
      $this->expectExceptionMessage('The client ID "a_non_existent_client" is invalid.');
    }

    $sap_order_id = $this->sapMiddlewareService->sendOrder($this->order, $client_id, $access_token);
    if ($expected) {
      $this->assertNotNull($sap_order_id);
      $this->assertEquals($expected, $sap_order_id);
    }
    else {
      $this->assertNull($sap_order_id);
    }
  }

  /**
   * Provides data to self::testSendOrder().
   */
  public function providerTestSendOrder() {
    return [
      [NULL, NULL, '10001'],
      [NULL, '123456', '10001'],
      ['another_client', NULL, '20002'],
      ['another_client', '789101', '20002'],
      ['exception_client', '101010', FALSE],
      ['a_non_existent_client', NULL, FALSE],
    ];
  }

  /**
   * @covers ::getOrdersToSync
   *
   * @dataProvider providerTestGetOrdersToSync
   */
  public function testGetOrdersToSync($args, $limit, $expected) {
    $query = $this->createMock(QueryInterface::class);
    $query->expects($this->exactly(count($args)))
      ->method('condition')
      ->will($this->returnValue($query));
    $query->expects($this->exactly(1))
      ->method('accessCheck')
      ->will($this->returnValue($query));
    $query->expects($this->exactly(1))
      ->method('range')
      ->with($this->equalTo(0, $limit))
      ->will($this->returnValue($query));
    $query->expects($this->exactly(1))
      ->method('execute')
      ->will($this->returnValue($expected));
    $entity_storage = $this->createMock(EntityStorageInterface::class);
    $entity_storage->expects($this->exactly(1))
      ->method('getQuery')
      ->will($this->returnValue($query));
    $log_storage = $this->prophesize(LogStorage::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getStorage('commerce_log')->willReturn($log_storage->reveal());
    $entity_type_manager->getStorage('commerce_order')->willReturn($entity_storage);

    $sap_middleware_service = new SapMiddlewareService(
      $this->loggerFactory->reveal(),
      $this->client->reveal(),
      $entity_type_manager->reveal(),
      $this->eventDispatcher->reveal(),
      $this->queueFactory->reveal(),
    );
    $order_ids = $sap_middleware_service->getOrdersToSync($args, $limit);
    $this->assertNotNull($order_ids);
    $this->assertEquals($expected, $order_ids);
  }

  /**
   * Provides data to self::testGetOrdersToSync().
   */
  public function providerTestGetOrdersToSync() {
    return [
      [
        [
          'field_some_order_field' => ['value' => 5, 'operator' => 'IN'],
        ],
        50,
        [1, 2, 3, 4, 5, 6],
      ],
      [
        [
          'field_some_order_field' => ['value' => 5, 'operator' => 'IN'],
          'field_another_field' => ['value' => 'test', 'operator' => 'IN'],
        ],
        50,
        [1, 4, 6],
      ],
      // With a limit.
      [
        [
          'field_some_order_field' => ['value' => 5, 'operator' => 'IN'],
        ],
        3,
        [1, 2, 3],
      ],
    ];
  }

  /**
   * Creates an SAP Middleware service object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createSapMiddlewareService() {
    // Mock logger factory and the Guzzle client and queue factory.
    $this->loggerFactory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $this->client = $this->prophesize(ClientInterface::class);
    $queue = $this->prophesize(QueueInterface::class);
    $queue->createItem(['order_id' => 1])->willReturn('101');
    $queue->createItem(['order_id' => 2])->willReturn('102');
    $this->queueFactory = $this->prophesize(QueueFactory::class);
    $this->queueFactory->get('sap_sync_orders')->willReturn($queue->reveal());

    // Mocks for entity type manager to return a client.
    // Mock a couple of client plugins.
    $this->order = $this->prophesize(OrderInterface::class);
    $this->order->id()->willReturn(1);
    $this->order->set('field_an_sap_id_field', '10001')->willReturn(1);
    $this->order->set('field_another_sap_id_field', '20002')->willReturn(1);
    $this->order->save()->willReturn(1);
    $this->order = $this->order->reveal();
    $payload = ['order' => $this->order];
    $sap_middleware_example_client_plugin = $this->prophesize(SapMiddlewareClientInterface::class);
    $sap_middleware_example_client_plugin->getAccessToken()->willReturn('123456');
    $sap_middleware_example_client_plugin->getSupportedStores()->willReturn([1]);
    $sap_middleware_example_client_plugin->getSupportedOrderTypes()->willReturn(['default']);
    $sap_middleware_example_client_plugin->getSapOrderIdField()->willReturn('field_an_sap_id_field');
    $sap_middleware_example_client_plugin->getOrderPayload($this->order)->willReturn($payload);
    $sap_middleware_example_client_plugin->sendOrder($payload, '123456')->willReturn(['sap_order_id' => '10001']);
    $sap_middleware_another_client_plugin = $this->prophesize(SapMiddlewareClientInterface::class);
    $sap_middleware_another_client_plugin->getAccessToken()->willReturn('789101');
    $sap_middleware_another_client_plugin->getSapOrderIdField()->willReturn('field_another_sap_id_field');
    $sap_middleware_another_client_plugin->getOrderPayload($this->order)->willReturn($payload);
    $sap_middleware_another_client_plugin->sendOrder($payload, '789101')->willReturn(['sap_order_id' => '20002']);
    // A client that will throw an exception when `sendOrder()` is called.
    $sap_middleware_exception_client_plugin = $this->prophesize(SapMiddlewareClientInterface::class);
    $sap_middleware_exception_client_plugin->getAccessToken()->willReturn('101010');
    $sap_middleware_exception_client_plugin->getOrderPayload($this->order)->willReturn($payload);
    $sap_middleware_exception_client_plugin->sendOrder($payload, '101010')->willThrow(new \RuntimeException('Failed to connect to server'));
    // Mock a couple of client entities.
    $sap_middleware_example_client_entity = $this->prophesize(SapMiddlewareClientEntityInterface::class);
    $sap_middleware_example_client_entity->id()->willReturn('example_client');
    $sap_middleware_example_client_entity->label()->willReturn('Example Client');
    $sap_middleware_example_client_entity->getPlugin()->willReturn($sap_middleware_example_client_plugin->reveal());
    $sap_middleware_another_client_entity = $this->prophesize(SapMiddlewareClientEntityInterface::class);
    $sap_middleware_another_client_entity->id()->willReturn('another_client');
    $sap_middleware_another_client_entity->label()->willReturn('Another Client');
    $sap_middleware_another_client_entity->getPlugin()->willReturn($sap_middleware_another_client_plugin->reveal());
    $sap_middleware_exception_client_entity = $this->prophesize(SapMiddlewareClientEntityInterface::class);
    $sap_middleware_exception_client_entity->id()->willReturn('exception_client');
    $sap_middleware_exception_client_entity->label()->willReturn('Exception Client');
    $sap_middleware_exception_client_entity->getPlugin()->willReturn($sap_middleware_exception_client_plugin->reveal());
    // Mock the entity storage.
    $query = $this->createMock(QueryInterface::class);
    $query->expects($this->any())
      ->method('condition')
      ->will($this->returnValue($query));
    $query->expects($this->any())
      ->method('accessCheck')
      ->will($this->returnValue($query));
    $query->expects($this->any())
      ->method('range')
      ->with($this->equalTo(0, 50))
      ->will($this->returnValue($query));
    $query->expects($this->any())
      ->method('execute')
      ->will($this->returnValue([1, 2]));
    $entity_storage = $this->prophesize(EntityStorageInterface::class);
    $entity_storage->loadByProperties(['status' => TRUE])->willReturn([$sap_middleware_example_client_entity->reveal()]);
    $entity_storage->getQuery('commerce_order')->willReturn($query);
    $entity_storage->load('another_client')->willReturn($sap_middleware_another_client_entity->reveal());
    $entity_storage->load('exception_client')->willReturn($sap_middleware_exception_client_entity->reveal());
    $entity_storage->load('a_non_existent_client')->willReturn(FALSE);
    $entity_storage->getQuery()->willReturn($query);
    $log_entity = $this->prophesize(LogInterface::class);
    $log_entity->save()->willReturn(1);
    $log_storage = $this->prophesize(LogStorage::class);
    $log_storage->generate($this->order, 'sap_middleware_order_sync_successful', [
      'order_id' => 1,
      'sap_order_id' => '10001',
    ])->willReturn($log_entity->reveal());
    $log_storage->generate($this->order, 'sap_middleware_order_sync_successful', [
      'order_id' => 1,
      'sap_order_id' => '20002',
    ])->willReturn($log_entity->reveal());
    $log_storage->generate($this->order, 'sap_middleware_order_sync_failed', [
      'order_id' => 1,
      'exception_message' => 'Failed to connect to server',
    ])->willReturn($log_entity->reveal());
    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->entityTypeManager->getStorage('sap_middleware_client')->willReturn($entity_storage->reveal());
    $this->entityTypeManager->getStorage('commerce_log')->willReturn($log_storage->reveal());
    $this->entityTypeManager->getStorage('commerce_order')->willReturn($entity_storage->reveal());
    // Mock the event dispatcher interface.
    $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

    // Finally, create the SAP Middleware service.
    $this->sapMiddlewareService = new SapMiddlewareService(
      $this->loggerFactory->reveal(),
      $this->client->reveal(),
      $this->entityTypeManager->reveal(),
      $this->eventDispatcher->reveal(),
      $this->queueFactory->reveal(),
    );
  }

}
