<?php

namespace Drupal\Tests\sap_middleware\Unit\Plugin\SapMiddleware\Client;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\sap_middleware\Annotation\SapMiddlewareClient;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\sap_middleware\Annotation\SapMiddlewareClient
 * @group sap_middleware
 */
class SapMiddlewareClientPluginTest extends UnitTestCase {

  /**
   * The mocked translator.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * {@inheritDoc}
   */
  public function setUp() {
    parent::setUp();

    $this->stringTranslation = $this->getStringTranslationStub();
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($container);
  }

  /**
   * @covers ::get
   *
   * @dataProvider providerTestGet
   */
  public function testGet(array $values) {
    $plugin = new SapMiddlewareClient($values);

    if (empty($values)) {
      $values += [
        'modes' => [
          'test' => new TranslatableMarkup('Test', [], [], $this->stringTranslation),
          'live' => new TranslatableMarkup('Live', [], [], $this->stringTranslation),
        ],
      ];
    }
    $this->assertEquals($values, $plugin->get());
  }

  /**
   * Provides data to self::testGet().
   */
  public function providerTestGet() {
    $data = [];
    $data[] = [
      [],
      [
        'modes' => [
          'test' => new TranslatableMarkup('Test', [], [], $this->stringTranslation),
        ],
      ],
    ];
    $data[] = [
      [
        'modes' => [
          'test' => new TranslatableMarkup('Test', [], [], $this->stringTranslation),
          'live' => new TranslatableMarkup('Live', [], [], $this->stringTranslation),
        ],
      ],
    ];

    return $data;
  }

  /**
   * @covers ::getProvider
   */
  public function testGetProvider() {
    $plugin = new SapMiddlewareClient(['provider' => 'example']);
    $this->assertEquals('example', $plugin->getProvider());
  }

  /**
   * @covers ::setProvider
   */
  public function testSetProvider() {
    $plugin = new SapMiddlewareClient([]);
    $plugin->setProvider('example');
    $this->assertEquals('example', $plugin->getProvider());
  }

  /**
   * @covers ::getId
   */
  public function testGetId() {
    $plugin = new SapMiddlewareClient(['id' => 'example']);
    $this->assertEquals('example', $plugin->getId());
  }

  /**
   * @covers ::getClass
   */
  public function testGetClass() {
    $plugin = new SapMiddlewareClient(['class' => 'example']);
    $this->assertEquals('example', $plugin->getClass());
  }

  /**
   * @covers ::setClass
   */
  public function testSetClass() {
    $plugin = new SapMiddlewareClient([]);
    $plugin->setClass('example');
    $this->assertEquals('example', $plugin->getClass());
  }

}
