<?php

namespace Drupal\Tests\sap_middleware\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\Core\Url;
use Drupal\sap_middleware\Entity\SapMiddlewareClient;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Test queueing orders and syncing them to SAP via the order queue worker.
 *
 * @coversDefaultClass \Drupal\sap_middleware\Plugin\QueueWorker\SyncOrders
 * @group sap_middleware
 */
class SyncOrderTest extends CommerceBrowserTestBase {
  use CronRunTrait;

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapMiddlewareClient;

  /**
   * The SAP Middleware Client entity.
   *
   * @var \Drupal\sap_middleware\Entity\SapMiddlewareClientInterface
   */
  protected $sapErrorMiddlewareClient;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_product',
    'commerce_order',
    'sap_middleware',
    'sap_middleware_client_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a standard order type and order item type.
    OrderType::create(
      [
        'id' => 'standard',
        'label' => 'Standard',
        'workflow' => 'order_default',
      ]
    )->save();
    OrderItemType::create(
      [
        'id' => 'standard',
        'label' => 'Standard',
        'orderType' => 'standard',
      ]
    )->save();

    // Create an SAP Middleware Client entity.
    $this->sapMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_middleware_client',
      'label' => 'Example Client',
      'plugin' => 'example_middleware_client',
      'status' => TRUE,
      'configuration' => [
        'mode' => 'test',
        'sync_orders_method' => 'cron_run',
        'stores_to_sync' => [1],
        'order_types_to_sync' => ['default'],
        'sap_order_id_field' => 'order_number',
      ],
    ]);
    $this->sapMiddlewareClient->save();

    // Create an SAP Middleware Client entity that will throw an error.
    $this->sapErrorMiddlewareClient = SapMiddlewareClient::create([
      'id' => 'example_error_middleware_client',
      'label' => 'Example Error Client',
      'plugin' => 'example_error_middleware_client',
      'status' => FALSE,
      'configuration' => [
        'mode' => 'test',
        'sync_orders_method' => 'cron_run',
        'stores_to_sync' => [1],
        'order_types_to_sync' => ['default'],
        'sap_order_id_field' => 'order_number',
      ],
    ]);
    $this->sapErrorMiddlewareClient->save();

    // Create sample orders.
    // Only 2 out of these orders should sync to SAP as the rest will fail the
    // filters that fetch the orders to sync.
    for ($i = 0; $i < 5; $i++) {
      // Make the 2nd order have a different order type to test that it's not
      // picked up in the cron run.
      $order_type = $i == 1 ? 'standard' : NULL;
      // Make the 3 order have a different store ID to test that it's not picked
      // up in the cron run.
      $store_id = $i == 2 ? 2 : NULL;
      // Make the 4th order have a value in the order_number field to test that
      // it's not picked up in the cron run.
      $order_number_value = $i == 3 ? '410001' : NULL;
      $this->createOrder($order_type, $store_id, $order_number_value);

      $this->drupalLogin($this->adminUser);
    }
  }

  /**
   * Test that the orders are synced successfully when cron is run.
   *
   * @covers \Drupal\sap_middleware\EventSubscriber\OrderSyncedSubscriber
   */
  public function testOrderSyncOnCron() {
    // Test there are 5 orders in the db.
    $orders = Order::loadMultiple();
    $this->assertCount(5, $orders);

    // Before running cron, ensure there are no items in the order queue.
    // Check whether no tasks are added to the queue.
    $queue = \Drupal::queue('sap_sync_orders', TRUE);
    $this->assertEquals(0, $queue->numberOfItems(), 'Queue is empty');

    $this->cronRun();

    // Now, ensure that exactly 3 orders have been synced with SAP. One order,
    // which was already synced, and the two orders that was just queued and
    // synced.
    $expected_synced_order_ids = [
      1,
      4,
      5,
    ];
    foreach ($orders as $order) {
      $order_id = $order->id();
      $this->drupalGet(Url::fromRoute('entity.commerce_order.canonical', [
        'commerce_order' => $order_id,
      ]));
      $this->assertSession()->statusCodeEquals(200);

      // If the order synced, it should have the SAP Order ID that was returned
      // from the sync as the order number field value via the order synced
      // subscriber.
      if (in_array($order_id, $expected_synced_order_ids)) {
        $sap_order_id = $order_id . '10001';
        $this->assertSession()->pageTextContains($sap_order_id);
        // The newly synced orders will have a log entered as well.
        if ($order_id != '4') {
          $this->assertSession()->pageTextContains("Order $order_id synced successfully with SAP. The order ID returned from SAP was $sap_order_id.");
        }
      }
      else {
        $this->assertSession()->pageTextNotContains($order_id . '10001');
      }
    }
  }

  /**
   * Test order sync failure on cron run.
   */
  public function testOrderSyncFailureOnCron() {
    // Enable the faulty client that throws an error and disable the other.
    $this->sapErrorMiddlewareClient->enable()->save();
    $this->sapMiddlewareClient->disable()->save();

    // Before running cron, ensure there are no items in the order queue.
    // Check whether no tasks are added to the queue.
    $queue = \Drupal::queue('sap_sync_orders', TRUE);
    $this->assertEquals(0, $queue->numberOfItems(), 'Queue is empty');

    $this->cronRun();

    // Now, ensure that exactly 3 orders have been synced with SAP. One order,
    // which was already synced, and the two orders that was just queued and
    // failed the sync.
    $expected_synced_order_ids = [
      1,
      4,
      5,
    ];
    $orders = Order::loadMultiple();
    foreach ($orders as $order) {
      $order_id = $order->id();
      $this->drupalGet(Url::fromRoute('entity.commerce_order.canonical', [
        'commerce_order' => $order_id,
      ]));
      $this->assertSession()->statusCodeEquals(200);

      // If the order sync failed, it should have a log added and no SAP order
      // ID.
      if (in_array($order_id, $expected_synced_order_ids)) {
        $sap_order_id = $order_id . '10001';
        if ($order_id != '4') {
          $this->assertSession()->pageTextNotContains($sap_order_id);
          $this->assertSession()->pageTextNotContains("Order $order_id synced successfully with SAP. The order ID returned from SAP was $sap_order_id.");
          $this->assertSession()->pageTextContains("Order $order_id failed syncing with SAP. The error returned was The specified access token is invalid.");
        }
      }
      else {
        $this->assertSession()->pageTextNotContains($order_id . '10001');
      }
    }
  }

  /**
   * Creates an order entity.
   *
   * @param mixed $order_type
   *   The order type.
   * @param mixed $store_id
   *   The store ID.
   * @param string|null $order_number
   *   The order number field will be used as the SAP order ID field.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  protected function createOrder($order_type = NULL, $store_id = NULL, string $order_number = NULL) {
    $order_item = $this->createEntity('commerce_order_item', [
      'type' => $order_type ?: 'default',
      'unit_price' => [
        'number' => '999',
        'currency_code' => 'USD',
      ],
    ]);
    $time = strtotime('now');
    $order = $this->createEntity('commerce_order', [
      'type' => $order_type ?: 'default',
      'mail' => $this->loggedInUser->getEmail(),
      'order_items' => [$order_item],
      'uid' => $this->loggedInUser,
      'store_id' => $store_id ?: $this->store,
      'state' => 'completed',
      'order_number' => $order_number ?: NULL,
      'placed' => $time,
      'completed' => $time,
    ]);
    $order->save();

    return $this->reloadEntity($order);
  }

}
