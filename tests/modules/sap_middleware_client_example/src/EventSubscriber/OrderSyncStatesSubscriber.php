<?php

namespace Drupal\sap_middleware_client_example\EventSubscriber;

use Drupal\sap_middleware\Event\OrderSyncStatesEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to the SAP order sync states event.
 */
class OrderSyncStatesSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderSyncStatesEvent::EVENT_NAME][] = ['onSyncStates'];
    return $events;
  }

  /**
   * Subscribes to the SAP order sync states event.
   *
   * @param \Drupal\sap_middleware\Event\OrderSyncStatesEvent $event
   *   The order payload event.
   */
  public function onSyncStates(OrderSyncStatesEvent $event) {
    $states = $event->getStates();
    $states[] = 'pending';
    $event->setStates($states);
  }

}
