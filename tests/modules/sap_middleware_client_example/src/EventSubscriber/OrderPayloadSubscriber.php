<?php

namespace Drupal\sap_middleware_client_example\EventSubscriber;

use Drupal\sap_middleware\Event\OrderPayloadEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to the SAP order payload event.
 */
class OrderPayloadSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderPayloadEvent::EVENT_NAME][] = ['onOrderPayload'];
    return $events;
  }

  /**
   * Subscribes to the SAP order payload event.
   *
   * @param \Drupal\sap_middleware\Event\OrderPayloadEvent $event
   *   The order payload event.
   */
  public function onOrderPayload(OrderPayloadEvent $event) {
    $payload = $event->getPayload();
    $payload['sap_customer_number'] = '1234';
    $event->setPayload($payload);
  }

}
