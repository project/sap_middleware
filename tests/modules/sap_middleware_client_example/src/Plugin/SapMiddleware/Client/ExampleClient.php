<?php

namespace Drupal\sap_middleware_client_example\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientBase;
use Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientInterface;

/**
 * Provides the Example Middleware Client.
 *
 * @SapMiddlewareClient(
 *   id = "example_middleware_client",
 *   label = "Example Middleware Client",
 * )
 */
class ExampleClient extends SapMiddlewareClientBase implements SapMiddlewareClientInterface {

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string {
    return '123456789';
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderPayload(OrderInterface $order): array {
    return [
      'order' => $order,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sendOrder(array $payload, $access_token) {
    $order = $payload['order'];
    return [
      'sap_order_id' => $order->id() . '10001',
    ];
  }

}
