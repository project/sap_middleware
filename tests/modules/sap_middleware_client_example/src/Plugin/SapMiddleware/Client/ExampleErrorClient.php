<?php

namespace Drupal\sap_middleware_client_example\Plugin\SapMiddleware\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientBase;
use Drupal\sap_middleware\Plugin\SapMiddleware\Client\SapMiddlewareClientInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Provides the Example Error Middleware Client.
 *
 * @SapMiddlewareClient(
 *   id = "example_error_middleware_client",
 *   label = "Example Error Middleware Client",
 * )
 */
class ExampleErrorClient extends SapMiddlewareClientBase implements SapMiddlewareClientInterface {

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string {
    return '123456789';
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderPayload(OrderInterface $order): array {
    return [
      'order' => $order,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sendOrder(array $payload, $access_token) {
    throw new HttpException(500, 'The specified access token is invalid');
  }

}
